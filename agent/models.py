# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

DictCommand = ['None', 'Takeoff', 'Land', 'GoHome', 'SetDirection']


class MainAxis(models.Model):
    main_axis = models.IntegerField(default=0)


class AgentFov(models.Model):
    agent_fov = models.CharField(max_length=200)


class TimeStampValidityPeriod(models.Model):
    time_stamp_validity_period = models.FloatField(default=0)


class Agent(models.Model):
    name = models.CharField(max_length=200, primary_key=True)
    rotation_x = models.FloatField(default=0)
    rotation_y = models.FloatField(default=0)
    rotation_z = models.FloatField(default=0)
    rotation_w = models.FloatField(default=0)
    translation_x = models.FloatField(default=0)
    translation_y = models.FloatField(default=0)
    translation_z = models.FloatField(default=0)
    time_stamp = models.DateTimeField(default=0)
    parent = models.CharField(max_length=200)
    # from prev agent
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    altitude = models.FloatField(default=0)
    azimuth = models.FloatField(default=0)
    armed = models.BooleanField(default=False)
    bat_volt = models.FloatField(default=0)
    gps_fix = models.IntegerField(default=0)
    gps_hdop = models.FloatField(default=0)
    mode = models.CharField(max_length=20)
    llc_message = models.CharField(max_length=200)
    llc_state = models.CharField(max_length=20)
    hlc_state = models.CharField(max_length=20)
    mc_state = models.CharField(max_length=20)

    def __str__(self):
        return "Agent " + str(self.name)


class AgentUidName(models.Model):
    uid = models.IntegerField(default=0, primary_key=True)
    name = models.CharField(max_length=200)


class Operator(models.Model):
    command = models.IntegerField()
    direction = models.FloatField('Azimuth in degrees')
    server_timestamp = models.DateTimeField('server reception time')

    def __str__(self):
        return DictCommand[self.command]

    def NoCommand(self):
        self.command = 0
        self.direction = -1
        self.server_timestamp = timezone.now()
        self.save()

    def Takeoff(self):
        self.command = 1
        self.direction = -1
        self.server_timestamp = timezone.now()
        self.save()

    def Land(self):
        self.command = 2
        self.direction = -1
        self.server_timestamp = timezone.now()
        self.save()

    def RTL(self):
        self.command = 3
        self.direction = -1
        self.server_timestamp = timezone.now()
        self.save()

    def SetDirection(self, direction):
        self.command = 4
        self.direction = direction
        self.server_timestamp = timezone.now()
        self.save()

    def isRelevant(self):
        timedeltaFromLastCommand = timezone.now() - self.server_timestamp
        return ((timedeltaFromLastCommand.seconds < 3) and (0 <= timedeltaFromLastCommand.seconds))