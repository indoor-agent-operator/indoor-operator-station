import pytz
from django.urls import reverse
from .models import Agent, AgentUidName, Operator, TimeStampValidityPeriod, AgentFov, MainAxis
import json


from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.reverse import reverse

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
import datetime
from scipy.spatial.transform import Rotation as R
import numpy as np
import math
from agent.serializers import AgentSerializer, OperatorSerializer

utc = pytz.UTC
agent_pov_cos = -1
main_axis = 'W'
axis = {'X': 0, 'Y': 1, 'Z': 2}

# ---------------------------------- Helper functions ---------------------------------


def get_latest_operator():
    if not Operator.objects.exists():
        user = Operator()
        user.NoCommand()
    else:
        user = Operator.objects.order_by('-server_timestamp')[0]
    return user


def detect_other_agents(agent):
    if not valide_time_stamp(agent):
        return False
    for other in Agent.objects.exclude(name=agent.name):
        if not valide_time_stamp(other):
            continue
        if not validate_same_parent(agent, other):
            continue
        cos_angle = calc_cos(agent, other)
        if agent_pov_cos <= cos_angle <= 1:
            return True
    return False


def valide_time_stamp(agent):
    time_stamp_validity_period = TimeStampValidityPeriod.objects.values_list('time_stamp_validity_period', flat=True)[0]
    time_stamp_validity_period = datetime.timedelta(seconds=time_stamp_validity_period)
    if datetime.datetime.now().replace(tzinfo=utc) > agent.time_stamp + time_stamp_validity_period:
        agent.delete()
        return False
    return True


def calc_cos(agent, other):
    # """
    # Finds the vec from agent to other in the labs' axis, transform it to the agents' axis,
    # and calculates the cos between this vector and the main axis of the agent
    # """
    set_agent_pov_cos()
    set_main_axis()
    vec_agent_other = np.array(
        [other.translation_x - agent.translation_x, other.translation_y - agent.translation_y,
         other.translation_z - agent.translation_z])
    trans_agent = R.from_quat(
        [-agent.rotation_x, -agent.rotation_y, -agent.rotation_z, agent.rotation_w])
    vec_other_in_agent = trans_agent.apply(vec_agent_other)
    return vec_other_in_agent[axis[main_axis]] / (
        np.sqrt(vec_other_in_agent[axis['X']] ** 2 + vec_other_in_agent[axis['Y']] ** 2 + vec_other_in_agent[
            axis['Z']] ** 2))


def set_agent_pov_cos():
    global agent_pov_cos
    if agent_pov_cos == -1:
        agent_pov = AgentFov.objects.values_list('agent_fov', flat=True)[0]
        agent_pov_cos = np.cos(math.radians(agent_pov / 2))


def set_main_axis():
    global main_axis
    if main_axis == 'W':
        main_axis = MainAxis.objects.values_list('main_axis', flat=True)[0]


def validate_same_parent(agent, other):
    if agent.parent != other.parent:
        return False
    return True


# --------------------------------- API ----------------------------------

class API(APIView):

    def get(self, request, format=None):
        reply = {
            'Takeoff': reverse('agents:takeoff', request=request, format=format),
            'Set Direction': reverse('agents:direction', request=request, format=format),
            'Land': reverse('agents:land', request=request, format=format),
            'Return to Launch': reverse('agents:rtl', request=request, format=format),
            'Last Operator Command': reverse('agents:command', args=('1',), request=request, format=format),
            'Agent List': reverse('agents:agent_list', request=request, format=format)
        }
        return Response(reply)


class LatestCommand(generics.RetrieveAPIView):
    """
    Latest Command Input
    """
    queryset = Operator.objects.all()
    serializer_class = OperatorSerializer


class AgentList(generics.ListAPIView):
    """
    A list of current active agents
    """
    queryset = Agent.objects.all()
    serializer_class = AgentSerializer


class Takeoff(APIView):
    """
    Operator Command to all listening agents to perform a takeoff
    """

    def post(self, request, format=None):
        user = get_latest_operator()
        user.Takeoff()
        return Response(status=200)


class SetDirection(APIView):
    """
    Operator Command to all listening agents to move towards some direction \
    (direction x in degrees, 0 <= x < 360) for 3 seconds.\
    Example : {"direction": 64}
    """

    def post(self, request, format=None):
        reply = Response(status=200)
        direction = 0
        try:
            direction = float(request.data['direction'])
        except Exception as e:
            reply = Response(data=e.__str__(), status=400)
        else:
            if ((0 <= direction) and (360 > direction)):
                user = get_latest_operator()
                user.SetDirection(direction)
            else:
                reply = Response(data='Azimuth out of bounds.', status=400)
        return reply


class Land(APIView):
    """
    Operator Command to all listening agents to land
    """

    def post(self, request, format=None):
        user = get_latest_operator()
        user.Land()
        return Response(status=200)


class ReturnToLaunch(APIView):
    """
    Operator Command to all listening agents to return to launch
    """

    def post(self, request, format=None):
        user = get_latest_operator()
        user.RTL()
        return Response(status=200)


# --------------------------------------------- UI -------------------------------------------

def users(request):
    return render(request, 'operator.html')


def set_direction(request):
    """
    Similar to update, just accepts a POST with a direction command.
    """
    reply = HttpResponse(status=405)
    if 'POST' == request.method:
        user = get_latest_operator()
        user.SetDirection(request.POST['direction'])
        reply = HttpResponse(status=405)
    return reply


# --------------------------------------- Agent -----------------------------------------------


@api_view(['POST'])
def agents(request):
    # """
    # Responds to an agent's POST request with the latest Operator's command.
    # """
    reply = HttpResponse(content='POST expected', status=405)
    if 'POST' == request.method:
        dict_message = json.loads(request.POST['root'])

        agent_uid = int(dict_message['Message']['Header']['Topic'])
        print(agent_uid)
        detected = False

        # Check if other agents are in this agent's field of view
        try:
            agent_uid_name = AgentUidName.objects.get(uid=agent_uid)
            agent_name = agent_uid_name.name
            agent = Agent.objects.get(name=agent_name)
        except:
            print("Agent does not exist")
        else:
            agent.latitude = float(dict_message['Message']['Body']['Lat']),
            agent.longitude = float(dict_message['Message']['Body']['Lon']),
            agent.altitude = float(dict_message['Message']['Body']['Alt']),
            agent.azimuth = float(dict_message['Message']['Body']['Yaw']),
            agent.armed = (1 == int(dict_message['Message']['Body']['Armed'])),
            agent.bat_volt = float(dict_message['Message']['Body']['Volt']),
            agent.gps_fix = int(dict_message['Message']['Body']['GpsFix']),
            agent.gps_hdop = float(dict_message['Message']['Body']['GpsHdop']),
            agent.mode = dict_message['Message']['Body']['Mode'],
            agent.llc_message = dict_message['Message']['Body']['LlcMessage'],
            agent.llc_state = dict_message['Message']['Body']['LlcState'],
            agent.hlc_state = dict_message['Message']['Body']['HlcState'],
            agent.mc_state = dict_message['Message']['Body']['McState']
            detected = detect_other_agents(agent)

        user = get_latest_operator()

        if not user.isRelevant():
            user.NoCommand()

        reply = {
            'id': agent_uid,
            'command': user.command,
            'direction': user.direction,
            'payload':
                {
                    'detected': detected
                },
            'timestamp': user.server_timestamp.__str__()
        }
        reply = HttpResponse(json.dumps(reply))
    return reply


# ---------------------------------- MocapProxy ---------------------------------


@api_view(['POST'])
def delete_agents(request):
    if 'POST' == request.method:
        Agent.objects.all().delete()
        return HttpResponseRedirect(reverse('agents:users'))
    else:
        return HttpResponse(content='POST expected')


@api_view(['POST'])
def get_agents_names(request):
    if 'POST' == request.method:
        agents_query = AgentUidName.objects.all().values()
        agents_names = []
        for agent in agents_query:
            agents_names.append(agent)
        return HttpResponse(json.dumps(agents_names, default=str))
    else:
        return HttpResponse(content='POST expected')


@api_view(['POST'])
def set_agents(request):
    if 'POST' == request.method:
        start = datetime.datetime.now()
        request = json.load(request)
        num_agents = len(request)
        for index in range(num_agents):
            agent = request[str(index)]
            name = agent['name']
            rotation_x = agent['rotation_x']
            rotation_y = agent['rotation_y']
            rotation_z = agent['rotation_z']
            rotation_w = agent['rotation_w']
            translation_x = agent['translation_x']
            translation_y = agent['translation_y']
            translation_z = agent['translation_z']
            time_stamp = datetime.datetime.now().replace(tzinfo=utc)
            parent = agent['parent']
            if Agent.objects.filter(name=name).exists():
                Agent.objects.filter(name=name).update(name=name, rotation_x=rotation_x, rotation_y=rotation_y,
                                                       rotation_z=rotation_z, rotation_w=rotation_w,
                                                       translation_x=translation_x,
                                                       translation_y=translation_y, translation_z=translation_z,
                                                       time_stamp=time_stamp, parent=parent)

            else:
                agent = Agent(name=name, rotation_x=rotation_x, rotation_y=rotation_y,
                              rotation_z=rotation_z, rotation_w=rotation_w, translation_x=translation_x,
                              translation_y=translation_y, translation_z=translation_z,
                              time_stamp=time_stamp, parent=parent)
                agent.save()
        return HttpResponseRedirect(reverse('agents:users'))
    else:
        return HttpResponse(content='POST expected')
