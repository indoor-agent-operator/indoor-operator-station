from django.contrib import admin

from .models import AgentUidName, Agent, TimeStampValidityPeriod, AgentFov, MainAxis

admin.site.register(Agent)
admin.site.register(AgentUidName)
admin.site.register(TimeStampValidityPeriod)
admin.site.register(AgentFov)
admin.site.register(MainAxis)
