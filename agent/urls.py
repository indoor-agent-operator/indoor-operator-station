
from django.urls import path, re_path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


app_name = 'agent'

urlpatterns = [
    path('api/', views.API.as_view()),
    path('api/takeoff/', views.Takeoff.as_view(), name='takeoff'),
    path('api/direction/', views.SetDirection.as_view(), name='direction'),
    path('api/land/', views.Land.as_view(), name='land'),
    path('api/rtl/', views.ReturnToLaunch.as_view(), name='rtl'),
    path('api/command/<int:pk>/', views.LatestCommand.as_view(), name='command'),
    path('api/list/', views.AgentList.as_view(), name='agent_list'),
    path('delete_agents/', views.delete_agents, name='delete_agents'),
    path('set_agents/', views.set_agents, name='set_agents'),
    path('get_agents_names/', views.get_agents_names, name='get_agents_names'),
    re_path(r'^agent/', views.agents, name='agents'),
    re_path(r'^ui/direction/', views.set_direction, name='set_direction'),
    re_path(r'^', views.users, name='users'),
]

urlpatterns = format_suffix_patterns(urlpatterns)


