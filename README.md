# Indoor Operator Station #


This is a [NeMALA Alate](https://gitlab.com/nemala/alate) companion project that implements [UCSLSV beahvior](https://ieeexplore.ieee.org/document/8267998) indoors.
The Alate agents call the server to let the operator know what they're up to, fetch operator commands, and answer if detected another agent in an agent's FOV or not.
The Operator receives agents' tfs, via a companion ROS project, [Agents Monitor](https://gitlab.com/indoor-agent-operator/agents_monitor), and uses geometry to determine if an agent detects another.



## <a name="dependencies"></a>Dependencies ##
| Dependency                        | Minimal Version   | License
| ------------                      | ---------------   | -------
| [Django](https://docs.djangoproject.com/en/2.2/intro/install/)   | 1.11.5 | [Django License](https://github.com/django/django/blob/master/LICENSE)
| [GeographicLib](https://geographiclib.sourceforge.io/html/index.html) | 1.49 | [MIT/X11](https://geographiclib.sourceforge.io/html/LICENSE.txt)
| [Markdown](https://pypi.python.org/pypi/Markdown) | 2.6.9 | [BSD](https://opensource.org/licenses/BSD-3-Clause)
| [Django REST](http://www.django-rest-framework.org/) | 3.6.4 | [Django REST](http://www.django-rest-framework.org/#license)
| [Django Filter](https://github.com/carltongibson/django-filter) | 1.0.4 | [Django Filter](https://github.com/carltongibson/django-filter/blob/develop/LICENSE)
| [Django Extensions](https://github.com/django-extensions/django-extensions) | 1.9.1 | [MIT](https://github.com/django-extensions/django-extensions/blob/master/LICENSE) 
| [scipy](https://github.com/scipy/scipy)                      | 1.9.3   | [scipy](https://github.com/scipy/scipy/blob/main/LICENSE.txt)
| [numpy](https://github.com/numpy/numpy)                      | 1.23.4   | [numpy](https://github.com/numpy/numpy/blob/main/LICENSE.txt)
| [pytz](https://github.com/newvem/pytz)                      | 2022.5   | [pytz](https://github.com/newvem/pytz/blob/master/LICENSE.txt)


# Deployment

1. Create a [virtual environment](https://docs.python.org/3/library/venv.html):
    ```console
    user@hostname:~/$ python3 -m venv Server
    ```
   
1. Activate the virtual environment:
    ```console
    user@hostname:~/$ source Server/bin/activate
    ```
   
1. Install Django to the virtual environment
    ```console
    (Server)user@hostname:~/$ cd path_to_project_root/
    (Server)user@hostname:~/path_to_project_root/$ pip3 install --no-cache-dir -r requirements.txt
    ```
       
1. Django Migrate:
    ```console
        (Server)user@hostname:~/path_to_project_root/$ python3 manage.py migrate
    ```
   
1. Create a superuser:
    ```console
        (Server)user@hostname:~/path_to_project_root/$ python3 manage.py createsuperuser
    ```    
1. Enter the following fields each step till successfully creating a superuser:
    ```console
        (Server)user@hostname:~/path_to_project_root/$ Username: admin
        (Server)user@hostname:~/path_to_project_root/$ Email address: admin@example.com
        (Server)user@hostname:~/path_to_project_root/$ Password: **********
        (Server)user@hostname:~/path_to_project_root/$ Password (again): *********
        (Server)user@hostname:~/path_to_project_root/$ Superuser created successfully.
    ```    
1. Run the server with the appropriate IP and port
    ```console
        (Server)user@hostname:~/path_to_project_root/$ python3 manage.py runserver 127.0.0.1:8000
    ```    
1. Open a Web browser and go to “/admin/” on your local domain http://127.0.0.1:8000/admin/.
1. Enter Username and Password.
1. Setup - Add the following data to the tables on the server:
    1. Add Agent fov. Enter the value '82.6' (FOV of tello drone).
    1. Add Agent uid names. Enter the names and uid of active agents.
    1. Add Main axis. The axis that is aligned with the agent's FOV center ('X', 'Y', 'Z').
    1. Add Time stamp validity period. The validity period for the agents tf samples.



# <a name="api"></a>Using the Indoor Operator Station
Now that everything is set up, open a browser and go to the address that the server serves, in these docs we used [127.0.0.1:8000](http://127.0.0.1:8000/).
1. Takeoff
1. Land
1. Return to launch
1. Azimuth knob and switch - when the switch is on a "set direction" command is sent to the server with the knob's azimuth.

